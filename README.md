* Install electron
``` shell
$ npm -g install electron-prebuilt
```

* clone repository
``` shell
$ git clone https://bitbucket.org/woolmark/sheep-electron/
```

* Run
``` shell
$ cd sheep-electron
$ electron .
```